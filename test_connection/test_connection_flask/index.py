from flask import Flask
from flask import request
from flask import url_for
from flask import redirect
from flask import render_template
from flask_mail import Mail, Message
import sqlite3
import os


def query_db(conn, query, args=(), one=False):
    cur = conn.execute(query, args)
    rv = [dict((cur.description[idx][0], value)
               for idx, value in enumerate(row)) for row in cur.fetchall()]
    return (rv[0] if rv else None) if one else rv


app = Flask(__name__)


@app.route('/wica/user/', methods=['GET', 'POST'])
def hello_world():       
   if request.method == 'POST':
      usr = request.form["username"]
      usr_pwd = request.form["password"]
      usr_email = request.form["email"]
      DATABASEDIR = os.getcwd() + '\\users.db'
      conn = sqlite3.connect(DATABASEDIR)
      userlist = query_db(conn, 'select * from users')
      # print(userlist)
      for i in userlist:
         if i["usr_email"] == usr_email:
            return redirect(url_for('old'))
         else:
            continue
      cur = conn.cursor()
      cur.execute("""INSERT INTO users (usr,usr_pwd,usr_email) 
            VALUES (?,?,?)""",(usr,usr_pwd,usr_email) )      
      conn.commit()
      print("add user")
      conn.close()        
      return redirect(url_for('new'))
   elif request.method == 'GET':
      return render_template("index.html")

@app.route('/wica/new/')
def new():
   return render_template("new.html")


@app.route('/wica/old/')
def old():
   return render_template("old.html")


@app.route('/wica/login/', methods=['GET', 'POST'])
def login():      
   if request.method == 'POST':
      usr = request.form["username"]
      usr_pwd = request.form["password"]
      DATABASEDIR = os.getcwd() + '\\users.db'
      conn = sqlite3.connect(DATABASEDIR)
      userlist = query_db(conn, 'select * from users')
      # print(userlist)
      conn.close()
      for i in userlist:
         if i["usr"] == usr and i["usr_pwd"] == usr_pwd:
            return "success"     
      return "fail"
   elif request.method == 'GET':
      return render_template("login.html")
 


if __name__ == '__main__':
   DATABASEDIR = os.getcwd() + '\\users.db'
   print(DATABASEDIR)
   conn = sqlite3.connect(DATABASEDIR)
   print("opened database")
   try:
      conn.execute('CREATE TABLE users (usr TEXT, usr_pwd TEXT, usr_email TEXT)')
      print("table created")
   except Exception as e:
      print(e)
      print("database exists")   
   # usr = "hyx"
   # usr_pwd = "12345678"
   # usr_email = "y.x.hu.hyx@gmail.com"
   # cur = conn.cursor()
   # cur.execute("""INSERT INTO users (usr,usr_pwd,usr_email) 
   #       VALUES (?,?,?)""",(usr,usr_pwd,usr_email) )      
   # conn.commit()
   # print("add user")
   # conn.close()        

   app.run(debug=True)


