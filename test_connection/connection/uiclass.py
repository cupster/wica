# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MyFrame1
###########################################################################

class MyFrame1 ( wx.Frame ):
	
    def __init__( self, parent ):
        wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"WICA Login", pos = wx.DefaultPosition, size = wx.Size( 320,240 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
        self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

        
        # self.icon1=wx.Icon(name="sunny.ico",type=wx.BITMAP_TYPE_ICO)
        # self.SetIcon(self.icon1)
		
        bSizer1 = wx.BoxSizer( wx.VERTICAL )
		
        sbSizer1 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"User Name" ), wx.VERTICAL )
		
        self.m_textCtrl1 = wx.TextCtrl( sbSizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        sbSizer1.Add( self.m_textCtrl1, 0, wx.ALL|wx.EXPAND, 5 )
		
		
        bSizer1.Add( sbSizer1, 1, wx.EXPAND, 5 )
		
        sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Password" ), wx.VERTICAL )
		
        self.m_textCtrl2 = wx.TextCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        sbSizer2.Add( self.m_textCtrl2, 0, wx.ALL|wx.EXPAND, 5 )
		
		
        bSizer1.Add( sbSizer2, 1, wx.EXPAND, 5 )
		
        self.m_button1 = wx.Button( self, wx.ID_ANY, u"Login", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_button1, 0, wx.ALL|wx.EXPAND, 5 )
        # bSizer1.Add( sbSizer2, 1, wx.EXPAND, 5 )
        self.m_button2 = wx.Button( self, wx.ID_ANY, u"Register", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_button2, 0, wx.ALL|wx.EXPAND, 5 )
		
		
        self.SetSizer( bSizer1 )
        self.Layout()
        self.m_menubar1 = wx.MenuBar( 0 )
        self.SetMenuBar( self.m_menubar1 )
		
		
        self.Centre( wx.BOTH )
		
		# Connect Events
        self.m_button1.Bind( wx.EVT_BUTTON, self.WicaLogin )
        self.m_button2.Bind( wx.EVT_BUTTON, self.Register )
	
    def __del__( self ):
        pass
	
	
	# Virtual event handlers, overide them in your derived class
    def WicaLogin( self, event ):
        event.Skip()
    
    def Register( self, event ):
        event.Skip()
	

